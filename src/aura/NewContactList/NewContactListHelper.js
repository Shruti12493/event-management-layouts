({
	drag : function(component, event) {
        var index = event.currentTarget.dataset.record;
        var record = component.get("v.contactList");
        var data = event.dataTransfer.setData("obj", JSON.stringify(record[index]));
	}, 
    
    ondragend : function(component, event){
        var contactList = component.get("v.contactList");
        var index = event.currentTarget.dataset.record;
        contactList.splice(index,1);
        component.set("v.contactList", contactList);
    }
})