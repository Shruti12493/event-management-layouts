public class SalesPath {
    
    @AuraEnabled
    public static Map<String, List<String>> getPicklistvalues(String recdId){
         if(recdId == null)
            return null;
        String objectAPIName = '';
        String keyPrefix = recdId.substring(0,3);
         for( Schema.SObjectType obj : Schema.getGlobalDescribe().Values() ){
              String prefix = obj.getDescribe().getKeyPrefix();
               if(prefix == keyPrefix){
                         objectAPIName = obj.getDescribe().getName();
                          break;
                }
         }
        Map<String, SalesPath__c> mapvalue = SalesPath__c.getAll();
        string pickListName = mapvalue.get(objectAPIName).Picklist__c;
        Map<String ,List<String>> picValMap = new Map<String ,List<String>>();
   		sObject sobjList = Database.query('SELECT '+pickListName+' FROM '+objectAPIName+' WHERE Id = :recdId');
        sObject obj =(sObject)sobjList;
        String currentVal= String.valueOf(obj.get(pickListName));
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectAPIName).getDescribe().Fields.getMap();
        Schema.SObjectField ft  = fMap.get(pickListName);
        Schema.DescribeFieldResult fd = ft.getDescribe(); 
        List<Schema.PicklistEntry> picklistValue= fd.getPicklistValues();         
        String flag = 'past';
        for(Schema.PicklistEntry picVal : picklistValue){
            
            if(picVal.getValue().equalsIgnoreCase(currentVal)){
                picValMap.put('current', new List<String>{picVal.getValue()});
                flag = 'future';
            }
            else if(flag == 'past'){
                if(picValMap.get('past')== null){
                     picValMap.put('past', new List<String>{picVal.getValue()});
                }else{
                    picValMap.get('past').add(picVal.getValue());
                }
            }
            else if (flag =='future'){
                if(picValMap.get('future') == null){
                 picValMap.put('future', new List<String>{picVal.getValue()});
                }else{
                    picValMap.get('future').add(picVal.getValue());
                }
            }
        }
        picValMap.put('objectName',new List<String>{objectAPIName});
        picValMap.put('fieldName',new List<String>{pickListName});
        return picValMap;
        
    }
    @AuraEnabled
    public static void saveInAccount(String valueToUpdate, String recdId, String objectName, String fieldName){
        sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject() ;
                sObj.put('id' , recdId) ;
                sObj.put(fieldName, valueToUpdate) ;
                update sObj ;
            }
    
}