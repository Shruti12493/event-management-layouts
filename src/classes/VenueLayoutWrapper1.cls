public class VenueLayoutWrapper1 {
    @AuraEnabled
    public String Seats {get; set;}
    @AuraEnabled
    public String Table {get; set;}
    @AuraEnabled
    public String LayoutType {get; set;}
	@AuraEnabled
    public String Seatperrow {get; set;}
	@AuraEnabled
    public String RowCount {get; set;}
	@AuraEnabled
    public String Noofsection {get; set;}
   	@AuraEnabled
    public String HorizontalSeats {get; set;}
    @AuraEnabled
    public String ConferenceType {get; set;}
    @AuraEnabled
    public String PositionX {get; set;}
    @AuraEnabled
    public String PositionY {get; set;}
    @AuraEnabled
    public String seatID {get; set;}
   
}